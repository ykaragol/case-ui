import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('findFare');
  this.route('dashboard');
  this.route('airport-search');
});

export default Router;
