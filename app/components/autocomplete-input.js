import Ember from 'ember';
import BsInput from 'ember-bootstrap/components/bs-input';

export default BsInput.extend({

  didInsertElement: function(){
    let that = this;
    this.$().autocomplete({
      source:function( request, response ) {
        return Ember.$.ajax({
          method:'GET',
          url: "/travel/searchAirports",
          data:{
            lang: Ember.get(that,'lang'),
            term: request.term
          },
          dataType: 'json',
          success: function(data){
            response(data);
          },
          error: function(jqXHR, textStatus, errorThrown){
            response();
          }
        });
      },
      minLength: 2,
      response: function(event, ui){
        if(ui.content && ui.content.map){
          ui.content.map(function(item){
            item.label=`${item.name} (${item.code})`;
            item.value=item.code;
          });
        }
      },
      change:function(event, ui){
       that.sendAction("valueChanged", ui.item);
      }
    });
  }
});
