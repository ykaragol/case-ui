import Ember from 'ember';

export default Ember.Component.extend({
  tableClassNames:'table table-striped table-bordered table-hover table-responsive table-condensed',

  currentPage : 1,
  paginatedData : Ember.computed.alias('airports'),
  pageSize : 10,

  lang:'en',

  triggerSearch(){
    let currentPage = this.get('currentPage');
    let pageSize = this.get('pageSize');
    let term = this.get('airportTerm');
    let lang = this.get('lang');
    this.sendAction("onSearch", term, lang, currentPage, pageSize);
  },

  actions:{
    next:function(){
      this.incrementProperty('currentPage');
      this.triggerSearch();
    },
    previous:function(){
      this.decrementProperty('currentPage');
      this.triggerSearch();
    },
    search(){
      this.triggerSearch();
    }
  }

});
