import Ember from 'ember';

export default Ember.Route.extend({
  model:function(){
    return new Ember.RSVP.Promise(function(resolve, reject) {
        Ember.$.ajax("/travel/klm-metrics").done(function(data){
          data["klm-metric-responseTimer_snapshot_mean"] = data["klm-metric-responseTimer.snapshot.mean"];
          data["klm-metric-responseTimer_snapshot_min"] = data["klm-metric-responseTimer.snapshot.min"];
          data["klm-metric-responseTimer_snapshot_max"] = data["klm-metric-responseTimer.snapshot.max"];
          resolve(data);
        }).fail(reject);
      }
    );
  }
});
