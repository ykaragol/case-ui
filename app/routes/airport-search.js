import Ember from 'ember';

export default Ember.Route.extend({

  init(){
    this.set('airports',[]);
  },

  model:function(){
    let airports = this.get('airports');
    return {
      airports:airports
    };
  },

  actions:{
    search:function(term, lang, pageIndex, pageSize){
      let that = this;
      that.get('airports').clear();

      Ember.$.ajax({
        method:'GET',
        url: "/travel/searchAirports",
        data:{
          lang: lang,
          term: term,
          page: pageIndex,
          size: pageSize
        },
        dataType: 'json'
      }).then((data)=>{
        that.get('airports').pushObjects(data);
      },()=>{
        alert("No data found!");
      });
    }
  }
});
