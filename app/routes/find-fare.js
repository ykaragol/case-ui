import Ember from 'ember';

export default Ember.Route.extend({

  fare:{
    data:{}
  },

  model:function(){
    return {
      fare: this.get('fare')
    };
  },

  actions:{
    findFare:function(){
      let origin = this.get('origin');
      let destination = this.get('destination');
      let that = this;
      that.set('fare.data.loading', true);
      Ember.$.ajax({
        url: "/travel/retrieveFare",
        data: { origin: origin, destination: destination }
      }).done(function( data ) {
        that.set('fare.data', data);
      }).always(function(){
        that.set('fare.data.loading', false);
      });
    },

    originChanged:function(value){
      this.set('origin', value ? value.code : value);
    },

    destinationChanged:function(value){
      this.set('destination', value ? value.code : value);
    }
  }
});
